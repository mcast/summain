/*
 * _summainmodule.c -- Python extensions for Summain
 *
 * Copyright (C) 2008, 2009  Lars Wirzenius <liw@liw.fi>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


#include <Python.h>


#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 600
#endif
#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

#ifdef __FreeBSD__
    #define NO_NANOSECONDS 1
    #include <sys/extattr.h>
#else
    #define NO_NANOSECONDS 0
    #include <sys/xattr.h>
#endif

/*
 * Since we can't set nanosecond mtime and atimes on some platforms, also
 * don't retrieve that level of precision from lstat(), so comparisons
 * work.
 */
static unsigned long long
remove_precision(unsigned long long nanoseconds)
{
#if NO_NANOSECONDS
    return nanoseconds - (nanoseconds % 1000);
#else
    return nanoseconds;
#endif
}


static PyObject *
lstat_wrapper(PyObject *self, PyObject *args)
{
    int ret;
    const char *filename;
    struct stat st = {0};

    if (!PyArg_ParseTuple(args, "s", &filename))
        return NULL;

    ret = lstat(filename, &st);
    if (ret == -1)
        ret = errno;

    return Py_BuildValue("iKKKKKKKLLLLKLKLK",
                         ret,
                         (unsigned long long) st.st_dev,
                         (unsigned long long) st.st_ino,
                         (unsigned long long) st.st_mode,
                         (unsigned long long) st.st_nlink,
                         (unsigned long long) st.st_uid,
                         (unsigned long long) st.st_gid,
                         (unsigned long long) st.st_rdev,
                         (long long) st.st_size,
                         (long long) st.st_blksize,
                         (long long) st.st_blocks,
                         (long long) st.st_atim.tv_sec,
                         remove_precision(st.st_atim.tv_nsec),
                         (long long) st.st_mtim.tv_sec,
                         remove_precision(st.st_mtim.tv_nsec),
                         (long long) st.st_ctim.tv_sec,
                         remove_precision(st.st_ctim.tv_nsec));
}


static PyObject *
llistxattr_wrapper(PyObject *self, PyObject *args)
{
    const char *filename;
    size_t bufsize;
    PyObject *o;
    char* buf;
    ssize_t n;

    if (!PyArg_ParseTuple(args, "s", &filename))
        return NULL;

#ifdef __FreeBSD__
    bufsize = extattr_list_link(filename, EXTATTR_NAMESPACE_USER, NULL, 0);
    buf = malloc(bufsize);
    n = extattr_list_link(filename, EXTATTR_NAMESPACE_USER, buf, bufsize);
    if (n >= 0) {
         /* Convert from length-prefixed BSD style to '\0'-suffixed
            Linux style. */
         size_t i = 0;
         while (i < n) {
             unsigned char length = (unsigned char) buf[i];
	     memmove(buf + i, buf + i + 1, length);
	     buf[i + length] = '\0';
	     i += length + 1;
	 }
	 o = Py_BuildValue("s#", buf, (int) n);
    } else {
         o = Py_BuildValue("i", errno);
    }
    free(buf);
#else
    bufsize = 0;
    o = NULL;
    do {
        bufsize += 1024;
        buf = malloc(bufsize);
        n = llistxattr(filename, buf, bufsize);

        if (n >= 0)
            o = Py_BuildValue("s#", buf, (int) n);
        else if (n == -1 && errno != ERANGE)
            o = Py_BuildValue("i", errno);
        free(buf);
    } while (o == NULL);
#endif
    return o;
}


static PyObject *
lgetxattr_wrapper(PyObject *self, PyObject *args)
{
    const char *filename;
    const char *attrname;
    size_t bufsize;
    PyObject *o;

    if (!PyArg_ParseTuple(args, "ss", &filename, &attrname))
        return NULL;

    bufsize = 0;
    o = NULL;
    do {
        bufsize += 1024;
        char *buf = malloc(bufsize);
#ifdef __FreeBSD__
	int n = extattr_get_link(filename, EXTATTR_NAMESPACE_USER, attrname, buf, bufsize);
#else
        ssize_t n = lgetxattr(filename, attrname, buf, bufsize);
#endif
        if (n >= 0)
            o = Py_BuildValue("s#", buf, (int) n);
        else if (n == -1 && errno != ERANGE)
            o = Py_BuildValue("i", errno);
        free(buf);
    } while (o == NULL);

    return o;
}


static PyObject *
lsetxattr_wrapper(PyObject *self, PyObject *args)
{
    const char *filename;
    const char *name;
    const char *value;
    int size;
    int ret;

    if (!PyArg_ParseTuple(args, "sss#", &filename, &name, &value, &size))
        return NULL;

#ifdef __FreeBSD__
    ret = extattr_set_link(filename, EXTATTR_NAMESPACE_USER, name, value, size);
#else
    ret = lsetxattr(filename, name, value, size, 0);
#endif
    if (ret == -1)
        ret = errno;
    return Py_BuildValue("i", ret);
}


static PyMethodDef methods[] = {
    {"lstat", lstat_wrapper, METH_VARARGS,
     "lstat(2) wrapper; arg is filename, returns tuple."},
    {"llistxattr", llistxattr_wrapper, METH_VARARGS,
     "llistxattr(2) wrapper; arg is filename, returns tuple."},
    {"lgetxattr", lgetxattr_wrapper, METH_VARARGS,
     "lgetxattr(2) wrapper; arg is filename, returns tuple."},
    {"lsetxattr", lsetxattr_wrapper, METH_VARARGS,
     "lsetxattr(2) wrapper; arg is filename, returns errno."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};


PyMODINIT_FUNC
init_summain(void)
{
    (void) Py_InitModule("_summain", methods);
}
