# Copyright 2011  Lars Wirzenius
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


all: _summain.so summain.1

_summain.so: _summainmodule.c setup.py
	python setup.py build_ext -i

summain.1: _summain.so summain.1.in
	python summain --generate-manpage=summain.1.in > summain.1

check:
	python -m CoverageTestRunner
	rm -f .coverage
	cmdtest tests
	pep8 summain summainlib.py summainlib_tests.py
	pylint --rcfile=pylint.conf summain summainlib.py summainlib_tests.py

clean:
	python setup.py clean
	rm -rf *.pyc .coverage summain.1 build _summain.so
