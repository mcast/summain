# setup.py - distutils module for Dimbola
# Copyright (C) 2009s  Lars Wirzenius <liw@liw.fi>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from distutils.core import setup, Extension

setup(name='summain',
      version='0.19',
      description='create file manifests with checksums',
      author='Lars Wirzenius',
      author_email='liw@liw.fi',
      url='http://liw.fi/summain/',
      py_modules=['summainlib'],
      scripts=['summain'],
      ext_modules=[Extension('_summain', sources=['_summainmodule.c'])],
      data_files=[
        ('share/man/man1', ['summain.1']),
      ],
     )

